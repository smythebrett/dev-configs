set expandtab
set shiftwidth=4
set softtabstop=4
syntax on
set autoindent
set colorcolumn=120
set number
colorscheme delek